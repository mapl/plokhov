package com.mapl.myapplication.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.google.android.material.bottomnavigation.BottomNavigationView.*
import com.mapl.myapplication.R
import com.mapl.myapplication.databinding.ActivityMainBinding
import com.mapl.myapplication.ui.gif.GifFragment.Mode.*
import com.mapl.myapplication.ui.gif.GifFragmentDirections
import com.mapl.myapplication.utils.viewBinding

class MainActivity : AppCompatActivity(), OnNavigationItemSelectedListener {
    private val binding by viewBinding(ActivityMainBinding::inflate)

    private lateinit var navController: NavController
    private var lastItem: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.navHostFragment) as NavHostFragment
        navController = navHostFragment.navController

        binding.navView.setOnNavigationItemSelectedListener(this)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        if (item.itemId != lastItem) {
            when (item.itemId) {
                R.id.actionRandom -> navController.navigate(
                    GifFragmentDirections.actionOtherMode(
                        RANDOM
                    )
                )
                R.id.actionTop -> navController.navigate(GifFragmentDirections.actionOtherMode(TOP))
            }
            lastItem = item.itemId
            return true
        }
        return false
    }
}
