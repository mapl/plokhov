package com.mapl.myapplication.ui.gif

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.koushikdutta.ion.Ion
import com.mapl.myapplication.R
import com.mapl.myapplication.databinding.FragmentGifBinding
import com.mapl.myapplication.utils.viewBinding
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import java.util.concurrent.CancellationException


class GifFragment : Fragment(R.layout.fragment_gif) {
    private val binding by viewBinding(FragmentGifBinding::bind)
    private val viewModel: GifViewModel by sharedViewModel()
    private val args: GifFragmentArgs by navArgs()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (viewModel.initIsComplete) viewModel.setMode(args.mode)

        binding.reloadButton.setOnClickListener {
            viewModel.setMode(args.mode)
        }

        binding.buttons.fabNext.setOnClickListener {
            viewModel.showNextGif()
        }

        binding.buttons.fabPrevious.setOnClickListener {
            viewModel.showPreviousGif()
        }

        viewModel.gifStateLD.observe(viewLifecycleOwner) { state ->
            state.loading.let {
                binding.progressBar.isVisible = it
                binding.tvGifDescription.isVisible = !it
                binding.ivGif.isVisible = !it
                binding.llError.isVisible = !it
            }

            state.error.let {
                binding.llError.isVisible = it
                binding.buttons.fabNext.isEnabled = !it
                binding.progressBar.isVisible = !it
                binding.tvGifDescription.isVisible = !it
                binding.ivGif.isVisible = !it
            }

            if (!state.loading && !state.error) {
                state.gif?.let { gif ->
                    binding.progressBar.isVisible = true
                    Ion.with(binding.ivGif)
                        .load(gif.gifURL)
                        .fail {
                            if (it !is CancellationException) {
                                viewModel.errorLoad()
                                binding.progressBar.isVisible = false
                            }
                        }
                        .success { binding.progressBar.isVisible = false }

                    binding.tvGifDescription.text = gif.description
                }
            }

            binding.buttons.fabPrevious.isEnabled =
                !(args.mode == Mode.RANDOM && viewModel.randomPos < 1 || args.mode == Mode.TOP && viewModel.topPos < 1)
        }
    }

    enum class Mode(val url: String) {
        RANDOM("random"), TOP("top")
    }
}
