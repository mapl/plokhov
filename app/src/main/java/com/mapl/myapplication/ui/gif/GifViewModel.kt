package com.mapl.myapplication.ui.gif

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mapl.myapplication.Gifs
import com.mapl.myapplication.NetworkApi
import com.mapl.myapplication.data.Gif
import com.mapl.myapplication.db.AppDao
import com.mapl.myapplication.ui.gif.GifFragment.*
import com.mapl.myapplication.utils.update
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber

class GifViewModel(private val networkApi: NetworkApi, private val dao: AppDao) : ViewModel() {
    private val gifState = MutableLiveData(GifViewState())
    val gifStateLD: LiveData<GifViewState> = gifState

    var initIsComplete = false

    private lateinit var randomGifs: List<Gif>
    private var topGifs: List<Gifs> = listOf()
    private var mode = Mode.RANDOM
    var randomPos: Int = -1
    var topPos: Int = -1

    init {
        viewModelScope.launch {
            gifState.update { it.copy(loading = true) }
            withContext(Dispatchers.IO) {
                randomGifs = dao.getGifs()
            }
            randomPos = randomGifs.lastIndex
            initIsComplete = true
            setMode(mode)
            gifState.update { it.copy(loading = false) }
        }
    }

    fun setMode(mode: Mode) {
        this.mode = mode

        when (mode) {
            Mode.RANDOM -> {
                if (randomPos >= 0) {
                    showGif(randomGifs, randomPos)
                } else {
                    ++randomPos
                    loadGif(mode.url)
                }
            }
            Mode.TOP -> {
                if (topPos >= 0) {
                    showGif(topGifs[topGifs.size / 5].result, topPos % 5)
                } else {
                    ++topPos
                    loadGif(mode.url, topPos)
                }
            }
        }
    }

    private fun showGif(list: List<Gif>, pos: Int) {
        viewModelScope.launch {
            try {
                val gif = list[pos]
                gifState.update { it.copy(gif = gif) }
            } catch (e: IndexOutOfBoundsException) {
                if (mode == Mode.RANDOM) {
                    loadGif(mode.url)
                } else if (mode == Mode.TOP) {
                    loadGif(mode.url, topPos)
                }
            }
        }
    }

    private fun loadGif(section: String) {
        viewModelScope.launch {
            gifState.update { it.copy(loading = true) }

            runCatching { networkApi.getGif("$section?json=true") }
                .onSuccess { response ->
                    Timber.v("loadGif response: %s", response)

                    withContext(Dispatchers.IO) {
                        dao.insertGif(response)
                    }

                    randomGifs = randomGifs.toMutableList() + response

                    gifState.update { it.copy(loading = false, gif = response, error = false) }
                }.onFailure { e ->
                    Timber.e("loadGif failed: %s", e.toString())
                    gifState.update { it.copy(loading = false, error = true) }
                }
        }
    }

    private fun loadGif(section: String, pos: Int) {
        viewModelScope.launch {
            gifState.update { it.copy(loading = true) }

            runCatching { networkApi.getGifs("$section/${pos / 5}?json=true") }
                .onSuccess { response ->
                    Timber.v("loadGif response: %s", response)

                    if (pos % 5 == 0) {
                        topGifs = topGifs.toMutableList() + response
                    }

                    val gif = topGifs[pos / 5].result[pos % 5]

                    gifState.update { it.copy(loading = false, gif = gif, error = false) }
                }.onFailure { e ->
                    Timber.e("loadGif failed: %s", e.toString())
                    gifState.update { it.copy(loading = false, error = true) }
                }
        }
    }

    fun errorLoad() {
        viewModelScope.launch {
            gifState.update { it.copy(error = true) }
        }
    }

    fun showNextGif() {
        viewModelScope.launch {
            if (mode == Mode.RANDOM) {
                if (randomPos < randomGifs.lastIndex) {
                    gifState.update {
                        it.copy(gif = randomGifs[++randomPos], error = false)
                    }
                } else {
                    ++randomPos
                    loadGif(mode.url)
                }
            } else if (mode == Mode.TOP) {
                if (topPos < topGifs.lastIndex) {
                    gifState.update {
                        it.copy(gif = topGifs[++topPos / 5].result[topPos % 5], error = false)
                    }
                } else {
                    ++topPos
                    loadGif(mode.url, topPos)
                }
            }
        }
    }

    fun showPreviousGif() {
        viewModelScope.launch {
            if (mode == Mode.RANDOM) {
                if (randomPos > 0) {
                    gifState.update {
                        it.copy(gif = randomGifs[--randomPos], error = false)
                    }
                }
            } else if (mode == Mode.TOP) {
                if (topPos > 0) {
                    gifState.update {
                        it.copy(gif = topGifs[--topPos / 5].result[topPos % 5], error = false)
                    }
                }
            }
        }
    }
}

data class GifViewState(
    val gif: Gif? = null,
    val loading: Boolean = false,
    val error: Boolean = false
)
