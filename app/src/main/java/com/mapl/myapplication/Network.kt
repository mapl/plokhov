package com.mapl.myapplication

import com.mapl.myapplication.data.Gif
import com.squareup.moshi.JsonClass
import retrofit2.http.GET
import retrofit2.http.Url

interface NetworkApi {
    @GET
    suspend fun getGif(@Url url: String): Gif

    @GET
    suspend fun getGifs(@Url url: String): Gifs
}

@JsonClass(generateAdapter = true)
data class Gifs(
    val result: List<Gif>
)
