package com.mapl.myapplication.data

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.JsonClass

@Entity
@JsonClass(generateAdapter = true)
data class Gif(
    val id: Long,
    val description: String,
    val gifURL: String
) {
    @PrimaryKey(autoGenerate = true)
    var uniqueId: Long = 0L
}
