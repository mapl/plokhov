package com.mapl.myapplication

import android.app.Application
import androidx.room.Room
import com.mapl.myapplication.db.AppDao
import com.mapl.myapplication.db.AppDatabase
import com.mapl.myapplication.ui.gif.GifViewModel
import com.squareup.moshi.Moshi
import okhttp3.OkHttpClient
import org.koin.android.ext.koin.androidApplication
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import timber.log.Timber

class App : Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@App)
            modules(apiModule, databaseModule, networkModule, viewModelModule)
        }

        Timber.plant(Timber.DebugTree())
        Timber.v("App started")
    }
}

val apiModule = module {
    fun provideAppApi(retrofit: Retrofit): NetworkApi {
        return retrofit.create(NetworkApi::class.java)
    }

    single { provideAppApi(get()) }
}

val databaseModule = module {
    fun provideDatabase(application: Application): AppDatabase {
        return Room.databaseBuilder(application, AppDatabase::class.java, "database")
            .build()
    }

    fun provideAppDao(database: AppDatabase): AppDao {
        return database.dao
    }

    single { provideDatabase(androidApplication()) }
    single { provideAppDao(get()) }
}

val networkModule = module {
    fun provideHttpClient(): OkHttpClient {
        return OkHttpClient.Builder()
//            .addNetworkInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
            .build()
    }

    fun provideRetrofit(client: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .baseUrl("https://developerslife.ru/")
            .client(client)
            .addConverterFactory(MoshiConverterFactory.create(Moshi.Builder().build()))
            .build()
    }

    single { provideHttpClient() }
    single { provideRetrofit(get()) }
}

val viewModelModule = module {
    viewModel { GifViewModel(get(), get()) }
}
