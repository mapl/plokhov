package com.mapl.myapplication.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.mapl.myapplication.data.Gif

@Dao
interface AppDao {
    @Query("SELECT * FROM Gif WHERE id = :id")
    fun getGif(id: Long): Gif

    @Query("SELECT * FROM Gif")
    fun getGifs(): List<Gif>

    @Insert(onConflict = REPLACE)
    fun insertGif(gif: Gif)
}
